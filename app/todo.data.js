(function(){
    'use strict';

    angular.module('todo.data', ['todo.data.api', 'todo.data.filter']);
})()