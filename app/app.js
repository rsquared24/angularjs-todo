
(function(){
    'use strict';

    angular.module('todo.app', ['ui.router', 'todo.component', 'todo.data'])
        .config(config);

    function config($stateProvider, $urlRouterProvider){
        $urlRouterProvider.otherwise("/todo/all");
    };

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
})();
