(function(){
    'use strict';
    
    var filterName = 'todoFilter';

    angular.module('todo.data.filter', [])
        .filter(filterName, filter);

    function filter($filter){
        function convert(value){
            var formatted = value.toLowerCase().trim();
            var rtn;

            switch(formatted) {
                case 'active':
                    rtn = false;
                    break;
                case 'completed':
                    rtn = true;
                    break;
                default:
                    rtn = null;
                    break;                            
            }

            return rtn;
        };

        return function(todos, type){
            if(!todos) {
                return;
            }
            
            var converted = convert(type);

            if(converted === null){
                return todos;
            }

            return $filter('filter')(todos, {'completed': converted});
        };
    };

    filter.$inject = ['$filter'];
})();