/**
 * A lot of the logic which manipulates the data here should be handled in some form of backend
 * system, however for the sake of completeness I have done the logic here.
 */
(function(){
    'use strict';

    var factoryName = 'todoApi';

    angular.module('todo.data.api', [])
        .factory(factoryName, factory)

    function factory($q, $filter){
        // private properties
        var data = [
            { id: 1, name: "Go outside", completed: true },
            { id: 2, name: "See the world", completed: false }
        ];
        var index = (data.length + 1);

        // private methods
        function promise(){
            var deferred = $q.defer();

            deferred.resolve({'data': data});

            return deferred.promise;
        };
        
        function insert(todo){
            // increment index then assign
            todo.id = ++index;

            // add to array
            data.push(todo);
        };

        function update(todo){
            // find the todo (if multiple get the first one)
            var found = $filter('filter')(data, {'id': todo.id})[0];

            // assign the updated one back to the one we found (if any)         
            found = todo;
        };

        // public methods
        var rtn = {};

        rtn.getAll = function(){
            return promise();
        };

        rtn.save = function(todos){
            if(!angular.isArray(todos)) {
                todos = [todos];
            }
            
            for(var i = 0, length = todos.length; i< length; i++){
                var todo = todos[i];

                if(!todo.id || todo.id <= 0){
                    insert(todo);
                }
                else{
                    update(todo)
                }
            };

            return promise();
        };

        rtn.delete = function(todo){  
            // loop through the todo list and attempt to find the one to delete                
            for(var i = 0, length = data.length; i < length; i++){
                // no match, continue
                if(data[i].id !== todo.id) {
                    continue;
                }

                // looks like we found it, remove it from the array and break out of the loop
                data.splice(i, 1);
                break;                
            }

            return promise();
        };

        return rtn;
    };

    factory.$inject = ['$q', '$filter'];
})();