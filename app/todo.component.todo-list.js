(function(){
    'use strict';

    var controllerName = 'todoCtrl'

    angular.module('todo.component.todoList', [])
        .config(config)
        .controller(controllerName, controller);
    
    function config($stateProvider){
        var route = {};

        route.url = '/todo/:type';
        route.templateUrl = 'todo.component.todo-list.tmpl.html';        
        route.controller = controllerName;
        route.controllerAs = 'vm';

        $stateProvider.state('todo', route);
    };

    config.$inject = ['$stateProvider'];

    function controller($scope, $filter, $stateParams, todoApi){
        // private properties
        var vm = this;

        // public properties
        vm.todoList = [];
        vm.noOfActive;
        vm.noOfCompleted;
        vm.todoNewText;
        vm.type = $stateParams.type;

        // private methods
        function constructor(){
            todoApi.getAll().then(success, error);
        };

        function success(resp){
            vm.todoList = resp.data;
        };

        function error(xhr, status, error){
            // implement
        };

        function filtered(completed){
            return $filter('filter')(vm.todoList, {'completed': completed});
        };

        // public methods
        vm.addTodo = function(todoNewText, e){
            if (e.which !== 13 || !todoNewText) {
                return;
            }

            var todoNew = {
                'name': todoNewText,
                'completed': false
            };
            
            todoApi.save(todoNew).then(success, error);

            vm.todoNewText = ''
        };

        vm.removeTodo = function(todo){
            todoApi.delete(todo).then(success, error);
        };

        vm.updateTodo = function(todo){
            todoApi.save(todo).then(success, error);
        };

        vm.noOfCompleted = function(){
            return filtered(true).length;
        };

        vm.noOfActive = function(){
            return filtered(false).length;
        };

        vm.checkAll = function(){
            var checked = (vm.noOfCompleted() !== vm.todoList.length);
            var toModify = filtered(!checked);
            
            for(var i = 0, length = toModify.length; i < length; i++){
                var item = toModify[i];
                item.completed = checked;
            }
        };

        vm.clearCompleted = function(){
            var completed = filtered(true);
            for(var i = 0, length = completed.length; i < length; i++){
                var item = completed[i];
                vm.removeTodo(item);
            }
        };

        // init
        constructor();
    };

    controller.$inject = ['$scope', '$filter', '$stateParams', 'todoApi'];
})();