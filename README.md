# AngularJS TodoMVC
This is a sample application of the TodoMVC written using AngularJS

## Prerequisites
[Node.JS](https://nodejs.org), [npm](https://npmjs.org) and a live internet connection are required to run this application. The internet connection is needed as some resources are pointing to a CDN.

## Commands
Navigate to the *node* folder and execute the following commands

### Installing the application

```
$ npm install
```

### Running the application

```
$ npm start
```