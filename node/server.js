var express = require('express');
var open = require('open');
var app = express();

var _appDirectory = '../app/';

app.use(express.static(_appDirectory));

app.get('/', function (req, res) {
  res.sendFile(_appDirectory + 'index.html');
});

app.listen(3000, function () {
  open('http://localhost:3000');
});

